<?php

class Talla extends myEloquent {    
    protected $table = 'my_cat_talla';
    protected $fillable = array('nombre', 'abrev');
}
