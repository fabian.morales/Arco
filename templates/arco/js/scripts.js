(function(window, $){         
    function scrollTo(target) {
        var wheight = $(window).height() / 2;
        
        var alto = $("#header").hasClass("header_fijo") ? $("#header").height() : $("#header").height() * 2;
        
        var ooo = $(target).offset().top - alto;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
	function ajustarVisibilidadMenu(){		
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + ($(window).height());
		var elemTop = $("#header").offset().top;
		var elemBottom = elemTop + $("#header").height();
        
		if ($("#header").height() <  docViewBottom && docViewTop > 0){                        
			$("#header").addClass("header_fijo");
		}
        else{
            $("#header").removeClass("header_fijo");
        }
	}
    
    $(document).ready(function() {
        $(window).resize(function() {                
            if ($("#top-menu-resp").is(":visible")){
                $("#header-top ul:first-child").hide();
            }
            else{                    
                $("#header-top ul:first-child").show();
            }
        });
        
        $(window).scroll(function () {
            ajustarVisibilidadMenu();
        });

        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul:first-child").toggle("slow");
        });
        
        $("#header-top ul li a, a[rel='nav-menu']").click(function(e){
            e.preventDefault();
            
            if ($(this).attr("href") === '/index.php' || $(this).attr("href") === 'index.php'){
                if ($("#quienes").length === 0){
                    window.location.href = '/index.php';
                }
                else{
                    scrollTo("#top-link");
                }
            }
            else{
                var $target = $($(this).attr("href"));
                if ($target.length === 0) {
                    window.location.href = '/index.php' + $(this).attr("href");
                }
                else{
                    scrollTo($target);    
                }    
            }
        });
        
        //$("#my_slider_2 a").featherlight();
        ajustarVisibilidadMenu();
        $(document).foundation();
	});
})(window, jQuery);